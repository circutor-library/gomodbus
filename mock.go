// Copyright (c) 2024 Circutor S.A. All rights reserved.

package gomodbus

import (
	"encoding/hex"
	"fmt"
	"strings"
	"testing"

	"github.com/Circutor/modbus"
)

type mockedFrame struct {
	response      string
	repeatability int
	totalCalls    int
}

type MockTransporter struct {
	frames            map[string]mockedFrame
	unexpectedRequest string
}

func NewMockTransporter() *MockTransporter {
	return &MockTransporter{
		frames:            make(map[string]mockedFrame),
		unexpectedRequest: "",
	}
}

func (mt *MockTransporter) Send(aduRequest []byte) (aduResponse []byte, err error) {
	requestEncoded := strings.ToUpper(hex.EncodeToString(aduRequest))

	frame, ok := mt.frames[requestEncoded]
	if !ok {
		mt.unexpectedRequest = requestEncoded

		return nil, fmt.Errorf("no response for request '%s'", requestEncoded)
	}

	decodedResponse, err := hex.DecodeString(frame.response)
	if err != nil {
		return nil, fmt.Errorf("failed to decode response '%s': %w", frame.response, err)
	}

	frame.totalCalls++
	mt.frames[requestEncoded] = frame

	return decodedResponse, nil
}

func (mt *MockTransporter) AddFrame(request, response string) {
	mt.frames[strings.ToUpper(request)] = mockedFrame{
		response:      strings.ToUpper(response),
		repeatability: 0,
		totalCalls:    0,
	}
}

func (mt *MockTransporter) AddFrameOnce(request, response string) {
	mt.frames[strings.ToUpper(request)] = mockedFrame{
		response:      strings.ToUpper(response),
		repeatability: 1,
		totalCalls:    0,
	}
}

func (mt *MockTransporter) AddFrameTimes(request, response string, times int) {
	mt.frames[strings.ToUpper(request)] = mockedFrame{
		response:      strings.ToUpper(response),
		repeatability: times,
		totalCalls:    0,
	}
}

func (mt *MockTransporter) AssertExpectations(t *testing.T) {
	t.Helper()

	for request, frame := range mt.frames {
		if frame.totalCalls == 0 {
			t.Errorf("request '%s' was not called", request)

			return
		}

		if frame.totalCalls != frame.repeatability && frame.repeatability != 0 {
			t.Errorf("request '%s' was called %d times, but expected %d", request, frame.totalCalls, frame.repeatability)

			return
		}
	}

	if mt.unexpectedRequest != "" {
		t.Errorf("unexpected request '%s'", mt.unexpectedRequest)

		return
	}
}

func NewMockModbusRTU(mt *MockTransporter, slaveID byte) *Modbus {
	rtuHandler := modbus.NewRTUClientHandler("")
	rtuHandler.SlaveId = slaveID

	//nolint:exhaustruct
	return &Modbus{
		client: modbus.NewClient2(rtuHandler, mt),
	}
}

func NewMockModbusTCP(mt *MockTransporter, slaveID byte) *Modbus {
	tcpHandler := modbus.NewTCPClientHandler("")
	tcpHandler.SlaveId = slaveID

	//nolint:exhaustruct
	return &Modbus{
		client: modbus.NewClient2(tcpHandler, mt),
	}
}
