// Copyright (c) 2024 Circutor S.A. All rights reserved.

package gomodbus

import (
	"fmt"
	"os"
	"time"
)

const (
	maxUpdateRetries = 3

	updateBlockSizeAddr    = 49951
	updateStartSendAddr    = 39998
	updateStartInstallAddr = 39999
)

func (m *Modbus) Update(fileName string) error {
	m.updatePercentage = 0

	m.isUpdating = true
	defer func() {
		m.isUpdating = false
	}()

	data, err := m.getFirmware(fileName)
	if err != nil {
		return err
	}

	if err := m.startUpdate(); err != nil {
		return err
	}

	blockSize, err := m.getBlockSize()
	if err != nil {
		return err
	}

	if err := m.sendBlocks(data, blockSize); err != nil {
		return err
	}

	if err := m.finishUpdate(); err != nil {
		return err
	}

	return nil
}

func (m *Modbus) IsUpdating() bool {
	return m.isUpdating
}

func (m *Modbus) UpdatePercentage() int {
	return m.updatePercentage
}

func (m *Modbus) getFirmware(fileName string) ([]byte, error) {
	data, err := os.ReadFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("binary file information could not be read: %w", err)
	}

	if len(data) < 1024 {
		return nil, fmt.Errorf("binary file too small: %w", err)
	}

	return data, nil
}

func (m *Modbus) startUpdate() error {
	// Increase timeout to allow the update to start (it takes a while)
	timeout := m.GetTimeout()

	m.SetTimeout(10 * time.Second)
	defer m.SetTimeout(timeout)

	if err := m.WriteCoil(updateStartSendAddr, true); err != nil {
		return fmt.Errorf("update could not be started: %w", err)
	}

	return nil
}

func (m *Modbus) getBlockSize() (int, error) {
	blockSize, err := m.ReadInputRegister(updateBlockSizeAddr)
	if err != nil {
		return 0, fmt.Errorf("update block size could not be read: %w", err)
	}

	return int(blockSize), nil
}

func (m *Modbus) sendBlocks(data []byte, blockSize int) error {
	numBlocks := len(data) / blockSize
	retries := 0

	for block := 0; block < numBlocks; block++ {
		m.updatePercentage = int((float64(block) / float64(numBlocks)) * 100)

		_, err := m.client.WriteFileRecord(uint16(block), 1, uint16(blockSize), []byte{0xFF, 0xFF}, data)
		if err != nil {
			if retries > maxUpdateRetries {
				return fmt.Errorf("update block %d could not be sent: %w", block, err)
			}

			retries++
			block--
		}
	}

	m.updatePercentage = 100

	return nil
}

func (m *Modbus) finishUpdate() error {
	if err := m.WriteCoil(updateStartInstallAddr, true); err != nil {
		return fmt.Errorf("update could not be activated: %w", err)
	}

	return nil
}
