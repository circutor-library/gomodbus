module gitlab.com/circutor-library/gomodbus

go 1.21

require (
	github.com/Circutor/modbus v0.1.7
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
