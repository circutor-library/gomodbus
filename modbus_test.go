// Copyright (c) 2024 Circutor S.A. All rights reserved.

package gomodbus_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/circutor-library/gomodbus"
)

func TestModbusRead(t *testing.T) {
	tests := map[string]struct {
		callFunc     func(*gomodbus.Modbus) (interface{}, error)
		sendFrame    string
		receiveFrame string
		expectedData interface{}
		shouldFail   bool
	}{
		"ReadInputRegister": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputRegister(123)
			},
			sendFrame:    "0104007B000141D3",
			receiveFrame: "0104020123F979",
			expectedData: uint16(0x0123),
			shouldFail:   false,
		},
		"ReadInputRegister - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputRegister(123)
			},
			sendFrame:    "0104007B000141D3",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputRegister - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputRegister(123)
			},
			sendFrame:    "0104007B000141D3",
			receiveFrame: "0104040123456778C8",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputBytes": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputBytes(246, 4)
			},
			sendFrame:    "010400F6000291F9",
			receiveFrame: "0104040123456778C8",
			expectedData: []byte{0x01, 0x23, 0x45, 0x67},
			shouldFail:   false,
		},
		"ReadInputBytes - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputBytes(246, 4)
			},
			sendFrame:    "010400F6000291F9",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputBytes - Invalid input": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputBytes(246, 3)
			},
			sendFrame:    "",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputRegisters": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputRegisters(246, 2)
			},
			sendFrame:    "010400F6000291F9",
			receiveFrame: "0104040123456778C8",
			expectedData: []uint16{0x0123, 0x4567},
			shouldFail:   false,
		},
		"ReadInputRegisters - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputRegisters(246, 2)
			},
			sendFrame:    "010400F6000291F9",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputRegisters - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputRegisters(246, 2)
			},
			sendFrame:    "010400F6000291F9",
			receiveFrame: "0104020123F979",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputUint32s": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputUint32s(1246, 2)
			},
			sendFrame:    "010404DE000490C3",
			receiveFrame: "0104080123456789ABCDEF82FE",
			expectedData: []uint32{0x01234567, 0x89ABCDEF},
			shouldFail:   false,
		},
		"ReadInputUint32s - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputUint32s(1246, 2)
			},
			sendFrame:    "010404DE000490C3",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputUint32s - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputUint32s(1246, 2)
			},
			sendFrame:    "010404DE000490C3",
			receiveFrame: "0104040123456778C8",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputUint64s": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputUint64s(2246, 2)
			},
			sendFrame:    "010408C600081391",
			receiveFrame: "0104100123456789ABCDEFFEDCBA9876543210FB06",
			expectedData: []uint64{0x0123456789ABCDEF, 0xFEDCBA9876543210},
			shouldFail:   false,
		},
		"ReadInputUint64s - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputUint64s(2246, 2)
			},
			sendFrame:    "010408C600081391",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputUint64s - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputUint64s(2246, 2)
			},
			sendFrame:    "010408C600081391",
			receiveFrame: "0104080123456789ABCDEF82FE",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputFloat32s": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputFloat32s(1246, 2)
			},
			sendFrame:    "010404DE000490C3",
			receiveFrame: "0104084144CCCD3E7BE76D2E05",
			expectedData: []float32{12.3, 0.246},
			shouldFail:   false,
		},
		"ReadInputFloat32s - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputFloat32s(1246, 2)
			},
			sendFrame:    "010404DE000490C3",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadInputFloat32s - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadInputFloat32s(1246, 2)
			},
			sendFrame:    "010404DE000490C3",
			receiveFrame: "0104040123456778C8",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingRegister": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingRegister(123)
			},
			sendFrame:    "0103007B0001F413",
			receiveFrame: "0103020123F80D",
			expectedData: uint16(0x0123),
			shouldFail:   false,
		},
		"ReadHoldingRegister - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingRegister(123)
			},
			sendFrame:    "0103007B0001F413",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingRegister - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingRegister(123)
			},
			sendFrame:    "0103007B0001F413",
			receiveFrame: "01030401234567797F",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingBytes": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingBytes(246, 4)
			},
			sendFrame:    "010300F600022439",
			receiveFrame: "01030401234567797F",
			expectedData: []byte{0x01, 0x23, 0x45, 0x67},
			shouldFail:   false,
		},
		"ReadHoldingBytes - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingBytes(246, 4)
			},
			sendFrame:    "010300F600022439",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingBytes - Invalid input": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingBytes(246, 3)
			},
			sendFrame:    "",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingRegisters": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingRegisters(246, 2)
			},
			sendFrame:    "010300F600022439",
			receiveFrame: "01030401234567797F",
			expectedData: []uint16{0x0123, 0x4567},
			shouldFail:   false,
		},
		"ReadHoldingRegisters - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingRegisters(246, 2)
			},
			sendFrame:    "010300F600022439",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingRegisters - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingRegisters(246, 2)
			},
			sendFrame:    "010300F600022439",
			receiveFrame: "0103020123F80D",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingUint32s": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingUint32s(1246, 2)
			},
			sendFrame:    "010304DE00042503",
			receiveFrame: "0103080123456789ABCDEF3324",
			expectedData: []uint32{0x01234567, 0x89ABCDEF},
			shouldFail:   false,
		},
		"ReadHoldingUint32s - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingUint32s(1246, 2)
			},
			sendFrame:    "010304DE00042503",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingUint32s - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingUint32s(1246, 2)
			},
			sendFrame:    "010304DE00042503",
			receiveFrame: "0103020123F80D",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingFloat32s": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingFloat32s(1246, 2)
			},
			sendFrame:    "010304DE00042503",
			receiveFrame: "0103084144CCCD3E7BE76D9FDF",
			expectedData: []float32{12.3, 0.246},
			shouldFail:   false,
		},
		"ReadHoldingFloat32s - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingFloat32s(1246, 2)
			},
			sendFrame:    "010304DE00042503",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadHoldingFloat32s - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadHoldingFloat32s(1246, 2)
			},
			sendFrame:    "010304DE00042503",
			receiveFrame: "0103020123F80D",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadDiscreteInput": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInput(532)
			},
			sendFrame:    "010202140001F876",
			receiveFrame: "010201016048",
			expectedData: true,
			shouldFail:   false,
		},
		"ReadDiscreteInput - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInput(532)
			},
			sendFrame:    "010202140001F876",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadDiscreteInput - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInput(532)
			},
			sendFrame:    "010202140001F876",
			receiveFrame: "010202010239E9",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadDiscreteInputs": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInputs(532, 4)
			},
			sendFrame:    "0102021400043875",
			receiveFrame: "01020106218A",
			expectedData: []bool{false, true, true, false},
			shouldFail:   false,
		},
		"ReadDiscreteInputs - Multiple bytes": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInputs(532, 16)
			},
			sendFrame:    "010202140010387A",
			receiveFrame: "01020265961286",
			expectedData: []bool{true, false, true, false, false, true, true, false, false, true, true, false, true, false, false, true},
			shouldFail:   false,
		},
		"ReadDiscreteInputs - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInputs(532, 4)
			},
			sendFrame:    "0102021400043875",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadDiscreteInputs - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadDiscreteInputs(532, 4)
			},
			sendFrame:    "0102021400043875",
			receiveFrame: "010202010239E9",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadCoil": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadCoil(12345)
			},
			sendFrame:    "01013039000122C7",
			receiveFrame: "010101019048",
			expectedData: true,
			shouldFail:   false,
		},
		"ReadCoil - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadCoil(12345)
			},
			sendFrame:    "01013039000122C7",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadCoil - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadCoil(12345)
			},
			sendFrame:    "01013039000122C7",
			receiveFrame: "010102010239AD",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadCoils": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadCoils(12345, 10)
			},
			sendFrame:    "01013039000A6300",
			receiveFrame: "0101024B020ECD",
			expectedData: []bool{true, true, false, true, false, false, true, false, false, true},
			shouldFail:   false,
		},
		"ReadCoils - No reply": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadCoils(12345, 10)
			},
			sendFrame:    "01013039000A6300",
			receiveFrame: "",
			expectedData: nil,
			shouldFail:   true,
		},
		"ReadCoils - Unexpected length": {
			callFunc: func(m *gomodbus.Modbus) (interface{}, error) {
				return m.ReadCoils(12345, 10)
			},
			sendFrame:    "01013039000A6300",
			receiveFrame: "0101030102032CEF",
			expectedData: nil,
			shouldFail:   true,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			tr := gomodbus.NewMockTransporter()
			tr.AddFrame(tc.sendFrame, tc.receiveFrame)

			m := gomodbus.NewMockModbusRTU(tr, 1)

			data, err := tc.callFunc(m)

			if tc.shouldFail {
				require.Error(t, err)

				return
			}

			require.NoError(t, err)
			require.Equal(t, tc.expectedData, data)

			tr.AssertExpectations(t)
		})
	}
}

func TestModbusWrite(t *testing.T) {
	tests := map[string]struct {
		callFunc     func(*gomodbus.Modbus, interface{}) error
		sendData     interface{}
		sendFrame    string
		receiveFrame string
		shouldFail   bool
	}{
		"WriteHoldingRegister": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingRegister(123, data.(uint16))
			},
			sendData:     uint16(0x0123),
			sendFrame:    "0110007B0001020123EC52",
			receiveFrame: "0110007B000171D0",
			shouldFail:   false,
		},
		"WriteHoldingRegister - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingRegister(123, data.(uint16))
			},
			sendData:     uint16(0x0123),
			sendFrame:    "0110007B0001020123EC52",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteHoldingBytes": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingBytes(1246, data.([]byte))
			},
			sendData:     []byte{0x01, 0x23, 0x45, 0x67},
			sendFrame:    "011004DE00020401234567CEF3",
			receiveFrame: "011004DE000220C2",
			shouldFail:   false,
		},
		"WriteHoldingBytes - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingBytes(1246, data.([]byte))
			},
			sendData:     []byte{0x01, 0x23, 0x45, 0x67},
			sendFrame:    "011004DE00020401234567CEF3",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteHoldingBytes - Invalid input": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingBytes(1246, data.([]byte))
			},
			sendData:     []byte{0x01, 0x23, 0x45},
			sendFrame:    "",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteHoldingRegisters": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingRegisters(1246, data.([]uint16))
			},
			sendData:     []uint16{0x0123, 0x4567},
			sendFrame:    "011004DE00020401234567CEF3",
			receiveFrame: "011004DE000220C2",
			shouldFail:   false,
		},
		"WriteHoldingRegisters - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingRegisters(1246, data.([]uint16))
			},
			sendData:     []uint16{0x0123, 0x4567},
			sendFrame:    "011004DE00020401234567CEF3",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteHoldingUint32s": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingUint32s(1246, data.([]uint32))
			},
			sendData:     []uint32{0x01234567, 0x89ABCDEF},
			sendFrame:    "011004DE0004080123456789ABCDEF3564",
			receiveFrame: "011004DE0004A0C0",
			shouldFail:   false,
		},
		"WriteHoldingUint32s - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingUint32s(1246, data.([]uint32))
			},
			sendData:     []uint32{0x01234567, 0x89ABCDEF},
			sendFrame:    "011004DE0004080123456789ABCDEF3564",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteHoldingFloat32s": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingFloat32s(1246, data.([]float32))
			},
			sendData:     []float32{12.3, 0.246},
			sendFrame:    "011004DE0004084144CCCD3E7BE76D999F",
			receiveFrame: "011004DE0004A0C0",
			shouldFail:   false,
		},
		"WriteHoldingFloat32s - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteHoldingFloat32s(1246, data.([]float32))
			},
			sendData:     []float32{12.3, 0.246},
			sendFrame:    "011004DE0004084144CCCD3E7BE76D999F",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteCoil - True": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteCoil(12345, data.(bool))
			},
			sendData:     true,
			sendFrame:    "01053039FF005337",
			receiveFrame: "01053039FF005337",
			shouldFail:   false,
		},
		"WriteCoil - False": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteCoil(12345, data.(bool))
			},
			sendData:     false,
			sendFrame:    "01053039000012C7",
			receiveFrame: "01053039000012C7",
			shouldFail:   false,
		},
		"WriteCoil - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteCoil(12345, data.(bool))
			},
			sendData:     true,
			sendFrame:    "01053039FF005337",
			receiveFrame: "",
			shouldFail:   true,
		},
		"WriteCoils": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteCoils(12345, data.([]bool))
			},
			sendData:     []bool{true, false, true, false, false, true, true, false, false, true},
			sendFrame:    "010F3039000A0265027AC3",
			receiveFrame: "010F3039000A0AC1",
			shouldFail:   false,
		},
		"WriteCoils - No reply": {
			callFunc: func(m *gomodbus.Modbus, data interface{}) error {
				return m.WriteCoils(12345, data.([]bool))
			},
			sendData:     []bool{true, false, true, false, false, true, true, false, false, true},
			sendFrame:    "010F3039000A0265027AC3",
			receiveFrame: "",
			shouldFail:   true,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			tr := gomodbus.NewMockTransporter()
			tr.AddFrame(tc.sendFrame, tc.receiveFrame)

			m := gomodbus.NewMockModbusRTU(tr, 1)

			err := tc.callFunc(m, tc.sendData)

			if tc.shouldFail {
				require.Error(t, err)

				return
			}

			require.NoError(t, err)
		})
	}
}
