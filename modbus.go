// Copyright (c) 2024 Circutor S.A. All rights reserved.

package gomodbus

import (
	"encoding/binary"
	"fmt"
	"log"
	"math"
	"net"
	"strconv"
	"time"

	"github.com/Circutor/modbus"
)

// Modbus struct.
type Modbus struct {
	client           modbus.Client
	rtuHandler       *modbus.RTUClientHandler
	tcpHandler       *modbus.TCPClientHandler
	isUpdating       bool
	updatePercentage int
}

func NewModbusRTU(serialPort string, baudRate int, slaveID byte, timeout time.Duration) *Modbus {
	rtuHandler := modbus.NewRTUClientHandler(serialPort)
	rtuHandler.BaudRate = baudRate
	rtuHandler.DataBits = 8
	rtuHandler.Parity = "N"
	rtuHandler.StopBits = 1
	rtuHandler.SlaveId = slaveID
	rtuHandler.Timeout = timeout

	return &Modbus{
		client:           modbus.NewClient(rtuHandler),
		rtuHandler:       rtuHandler,
		tcpHandler:       nil,
		isUpdating:       false,
		updatePercentage: 0,
	}
}

func NewModbusTCP(ip string, port int, slaveID byte, timeout time.Duration) *Modbus {
	tcpHandler := modbus.NewTCPClientHandler(net.JoinHostPort(ip, strconv.Itoa(port)))
	tcpHandler.Timeout = timeout
	tcpHandler.SlaveId = slaveID

	return &Modbus{
		client:           modbus.NewClient(tcpHandler),
		rtuHandler:       nil,
		tcpHandler:       tcpHandler,
		isUpdating:       false,
		updatePercentage: 0,
	}
}

func (m *Modbus) SetLogger(logger *log.Logger) {
	if m.rtuHandler != nil {
		m.rtuHandler.Logger = logger
	}

	if m.tcpHandler != nil {
		m.tcpHandler.Logger = logger
	}
}

func (m *Modbus) SetTimeout(timeout time.Duration) {
	if m.rtuHandler != nil {
		m.rtuHandler.Close()
		m.rtuHandler.Timeout = timeout
	}

	if m.tcpHandler != nil {
		m.tcpHandler.Close()
		m.tcpHandler.Timeout = timeout
	}
}

func (m *Modbus) GetTimeout() time.Duration {
	if m.rtuHandler != nil {
		return m.rtuHandler.Timeout
	}

	if m.tcpHandler != nil {
		return m.tcpHandler.Timeout
	}

	return 0
}

func (m *Modbus) ReadInputRegister(address uint16) (uint16, error) {
	data, err := m.client.ReadInputRegisters(address, 1)
	if err != nil {
		return 0, fmt.Errorf("failed read input register in address %d: %w", address, err)
	}

	if len(data) != 2 {
		return 0, fmt.Errorf("received %d bytes, expected 2", len(data))
	}

	return binary.BigEndian.Uint16(data), nil
}

func (m *Modbus) ReadInputBytes(address uint16, quantity uint16) ([]byte, error) {
	if quantity%2 != 0 {
		return nil, fmt.Errorf("quantity must be multiple of 2")
	}

	data, err := m.client.ReadInputRegisters(address, quantity/2)
	if err != nil {
		return nil, fmt.Errorf("failed read %d input bytes in address %d: %w", quantity, address, err)
	}

	return data, nil
}

func (m *Modbus) ReadInputRegisters(address, quantity uint16) ([]uint16, error) {
	data, err := m.client.ReadInputRegisters(address, quantity)
	if err != nil {
		return nil, fmt.Errorf("failed read %d input registers in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*2) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*2)
	}

	regs := make([]uint16, quantity)
	for i := 0; i < len(data); i += 2 {
		regs[i/2] = binary.BigEndian.Uint16(data[i:])
	}

	return regs, nil
}

func (m *Modbus) ReadInputUint32s(address, quantity uint16) ([]uint32, error) {
	data, err := m.client.ReadInputRegisters(address, quantity*2)
	if err != nil {
		return nil, fmt.Errorf("failed read %d input uint32 in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*4) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*4)
	}

	values := make([]uint32, quantity)
	for i := 0; i < len(data); i += 4 {
		values[i/4] = binary.BigEndian.Uint32(data[i:])
	}

	return values, nil
}

func (m *Modbus) ReadInputUint64s(address, quantity uint16) ([]uint64, error) {
	data, err := m.client.ReadInputRegisters(address, quantity*4)
	if err != nil {
		return nil, fmt.Errorf("failed read %d input uint64 in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*8) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*8)
	}

	values := make([]uint64, quantity)
	for i := 0; i < len(data); i += 8 {
		values[i/8] = binary.BigEndian.Uint64(data[i:])
	}

	return values, nil
}

func (m *Modbus) ReadInputFloat32s(address, quantity uint16) ([]float32, error) {
	data, err := m.client.ReadInputRegisters(address, quantity*2)
	if err != nil {
		return nil, fmt.Errorf("failed read %d input float32 in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*4) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*4)
	}

	values := make([]float32, quantity)
	for i := 0; i < len(data); i += 4 {
		values[i/4] = math.Float32frombits(binary.BigEndian.Uint32(data[i:]))
	}

	return values, nil
}

func (m *Modbus) ReadHoldingRegister(address uint16) (uint16, error) {
	data, err := m.client.ReadHoldingRegisters(address, 1)
	if err != nil {
		return 0, fmt.Errorf("failed read holding register in address %d: %w", address, err)
	}

	if len(data) != 2 {
		return 0, fmt.Errorf("received %d bytes, expected 2", len(data))
	}

	return binary.BigEndian.Uint16(data), nil
}

func (m *Modbus) ReadHoldingBytes(address uint16, quantity uint16) ([]byte, error) {
	if quantity%2 != 0 {
		return nil, fmt.Errorf("quantity must be multiple of 2")
	}

	data, err := m.client.ReadHoldingRegisters(address, quantity/2)
	if err != nil {
		return nil, fmt.Errorf("failed read %d holding bytes in address %d: %w", quantity, address, err)
	}

	return data, nil
}

func (m *Modbus) ReadHoldingRegisters(address, quantity uint16) ([]uint16, error) {
	data, err := m.client.ReadHoldingRegisters(address, quantity)
	if err != nil {
		return nil, fmt.Errorf("failed read %d holding registers in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*2) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*2)
	}

	regs := make([]uint16, quantity)
	for i := 0; i < len(data); i += 2 {
		regs[i/2] = binary.BigEndian.Uint16(data[i:])
	}

	return regs, nil
}

func (m *Modbus) ReadHoldingUint32s(address, quantity uint16) ([]uint32, error) {
	data, err := m.client.ReadHoldingRegisters(address, quantity*2)
	if err != nil {
		return nil, fmt.Errorf("failed read %d holding uint32 in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*4) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*4)
	}

	values := make([]uint32, quantity)
	for i := 0; i < len(data); i += 4 {
		values[i/4] = binary.BigEndian.Uint32(data[i:])
	}

	return values, nil
}

func (m *Modbus) ReadHoldingFloat32s(address, quantity uint16) ([]float32, error) {
	data, err := m.client.ReadHoldingRegisters(address, quantity*2)
	if err != nil {
		return nil, fmt.Errorf("failed read %d holding float32 in address %d: %w", quantity, address, err)
	}

	if len(data) != int(quantity*4) {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), quantity*4)
	}

	values := make([]float32, quantity)
	for i := 0; i < len(data); i += 4 {
		values[i/4] = math.Float32frombits(binary.BigEndian.Uint32(data[i:]))
	}

	return values, nil
}

func (m *Modbus) WriteHoldingRegister(address, value uint16) error {
	data := make([]byte, 2)
	binary.BigEndian.PutUint16(data, value)

	if _, err := m.client.WriteMultipleRegisters(address, 1, data); err != nil {
		return fmt.Errorf("failed write holding register in address %d: %w", address, err)
	}

	return nil
}

func (m *Modbus) WriteHoldingBytes(address uint16, values []byte) error {
	if (len(values) % 2) != 0 {
		return fmt.Errorf("values length must be multiple of 2")
	}

	if _, err := m.client.WriteMultipleRegisters(address, uint16(len(values)/2), values); err != nil {
		return fmt.Errorf("failed write %d holding bytes in address %d: %w", len(values), address, err)
	}

	return nil
}

func (m *Modbus) WriteHoldingRegisters(address uint16, values []uint16) error {
	data := make([]byte, len(values)*2)

	for i := 0; i < len(values); i++ {
		binary.BigEndian.PutUint16(data[i*2:], values[i])
	}

	if _, err := m.client.WriteMultipleRegisters(address, uint16(len(values)), data); err != nil {
		return fmt.Errorf("failed write %d holding registers in address %d: %w", len(values), address, err)
	}

	return nil
}

func (m *Modbus) WriteHoldingUint32s(address uint16, values []uint32) error {
	data := make([]byte, len(values)*4)

	for i := 0; i < len(values); i++ {
		binary.BigEndian.PutUint32(data[i*4:], values[i])
	}

	if _, err := m.client.WriteMultipleRegisters(address, uint16(len(values)*2), data); err != nil {
		return fmt.Errorf("failed write %d holding uint32 in address %d: %w", len(values), address, err)
	}

	return nil
}

func (m *Modbus) WriteHoldingFloat32s(address uint16, values []float32) error {
	data := make([]byte, len(values)*4)

	for i := 0; i < len(values); i++ {
		binary.BigEndian.PutUint32(data[i*4:], math.Float32bits(values[i]))
	}

	if _, err := m.client.WriteMultipleRegisters(address, uint16(len(values)*2), data); err != nil {
		return fmt.Errorf("failed write %d holding float32 in address %d: %w", len(values), address, err)
	}

	return nil
}

func (m *Modbus) ReadDiscreteInput(address uint16) (bool, error) {
	data, err := m.client.ReadDiscreteInputs(address, 1)
	if err != nil {
		return false, fmt.Errorf("failed read discrete input in address %d: %w", address, err)
	}

	if len(data) != 1 {
		return false, fmt.Errorf("received %d bytes, expected 1", len(data))
	}

	return data[0] == 1, nil
}

func (m *Modbus) ReadDiscreteInputs(address, quantity uint16) ([]bool, error) {
	data, err := m.client.ReadDiscreteInputs(address, quantity)
	if err != nil {
		return nil, fmt.Errorf("failed read %d discrete inputs in address %d: %w", quantity, address, err)
	}

	expectedBytes := int(math.Ceil(float64(quantity) / 8))
	if len(data) != expectedBytes {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), expectedBytes)
	}

	dataLen := int(quantity)
	status := make([]bool, quantity)

	for inputIndex := 0; inputIndex < dataLen; inputIndex++ {
		bytePos := inputIndex / 8
		bitMask := byte(1 << (inputIndex % 8))

		if (data[bytePos] & bitMask) != 0 {
			status[inputIndex] = true
		}
	}

	return status, nil
}

func (m *Modbus) ReadCoil(address uint16) (bool, error) {
	data, err := m.client.ReadCoils(address, 1)
	if err != nil {
		return false, fmt.Errorf("failed read coil in address %d: %w", address, err)
	}

	if len(data) != 1 {
		return false, fmt.Errorf("received %d bytes, expected 1", len(data))
	}

	return data[0] == 1, nil
}

func (m *Modbus) ReadCoils(address, quantity uint16) ([]bool, error) {
	data, err := m.client.ReadCoils(address, quantity)
	if err != nil {
		return nil, fmt.Errorf("failed read %d coils in address %d: %w", quantity, address, err)
	}

	expectedBytes := int(math.Ceil(float64(quantity) / 8))
	if len(data) != expectedBytes {
		return nil, fmt.Errorf("received %d bytes, expected %d", len(data), expectedBytes)
	}

	dataLen := int(quantity)
	status := make([]bool, quantity)

	for coilIndex := 0; coilIndex < dataLen; coilIndex++ {
		bytePos := coilIndex / 8
		bitMask := byte(1 << (coilIndex % 8))

		if (data[bytePos] & bitMask) != 0 {
			status[coilIndex] = true
		}
	}

	return status, nil
}

func (m *Modbus) WriteCoil(address uint16, value bool) error {
	data := uint16(0x0000)
	if value {
		data = 0xFF00
	}

	if _, err := m.client.WriteSingleCoil(address, data); err != nil {
		return fmt.Errorf("failed write coil in address %d: %w", address, err)
	}

	return nil
}

func (m *Modbus) WriteCoils(address uint16, values []bool) error {
	data := make([]byte, (1 + len(values)/8))

	for i, value := range values {
		if value {
			data[i/8] |= 1 << (i % 8)
		}
	}

	if _, err := m.client.WriteMultipleCoils(address, uint16(len(values)), data); err != nil {
		return fmt.Errorf("failed write %d coils in address %d: %w", len(values), address, err)
	}

	return nil
}
